/**
 * 1. Напиши функцию createCounter, которая будет считать количество вызовов.
 * Нужно использовать замыкание.
 */

function createCounter() {
    let counter = 0;
    return () => ++counter;
}

/**
 * 2. Не меняя уже написаный код (можно только дописывать новый),
 * сделай так, чтобы в calculateHoursOnMoon
 * переменная HOURS_IN_DAY была равна 29,5, а в функции calculateHoursOnEarth
 * эта переменная была 24.
 */

let HOURS_IN_DAY = 24;

function calculateHoursOnMoon (days) {
    let HOURS_IN_DAY = 29.5;
    return days * HOURS_IN_DAY;
}

function calculateHoursOnEarth (days) {
    return days * HOURS_IN_DAY;
}

/**
 * 3. Допиши функцию crashMeteorite, после которой
 * продолжительность дня на земле (из предыдущей задачи)
 * изменится на 22 часа
 */

function crashMeteorite () {
    HOURS_IN_DAY = 22;
}

/**
 * 4. Функция createMultiplier должна возвращать функцию, которая
 * при первом вызове возвращает произведение аргумента
 * функции createMultiplier и переменной a, при втором — аргумента и b,
 * при третьем — аргумента и c, с четвертого вызова снова a, потом b и так далее.
 *
 * Например:
 * const func = createMultiplier(2);
 * func(); // 16 (2*a)
 * func(); // 20 (2*b)
 * func(); // 512 (2*c)
 * func(); // 16 (2*a)
 *
 */
function createMultiplier (num) {
    const a = 8;
    const b = 10;
    const c = 256;
    let counter = 0;
    return () => {
        switch (counter) {
            case 0:
                counter++;
                return a * num;
            case 1:
                counter++;
                return b * num;
            case 2:
                counter = 0;
                return c * num;
        }
    }
}

/**
 * 5. Напиши функцию createStorage, которая будет уметь хранить
 * какие-то данные и манипулировать ими.
 * Функция должна возвращать объект с методами:
 * - add — метод, который принимает на вход любое количество аргументов и добавляет их в хранилище;
 * - get — метод, возвращающий хранилище;
 * - clear — метод, очищающий хранилище;
 * - remove — метод, который принимает на вход элемент, который нужно удалить из хранилища и удаляет его;
 *
 * Примеры использования смотри в тестах.
 */

function createStorage () {
    let storage = [];
    return {
        add: function () {
            storage.push(...arguments);
        },
        get: function () {
            return storage
        },
        clear: function () {
            storage = [];
        },
        remove: function (forRemove) {
            storage = storage.filter(elem => elem !== forRemove)
        }
    }
}

/**
 * 6*. Реализовать через let функцию поочередного
 * добавления в массив чисел от 0 до 10 с интервалом в 500ms.
 *
 * Для выполнения задачи должен быть цикл от 0 до 10,
 * внутри которого должен быть setTimeout(func, 50) с функцией внутри,
 * которая должна наполнить массив result числами от 0 до 10
*/

function letTimeout () {
    const result = [];


    for(let i = 0; i <= 10; i++) {
        setTimeout(() => result.push(i), 500);
    }

    return result; // числа от 0 до 10
}

/**
 * 7*. Реализовать такую же функцию, как letTimeout, только через var.
 * В комментарии объяснить, почему и как она работает
 */
// Как я понял, так как let i виден только внутри блока for и изменяется внутри этого блока от 0 до 11, 
// result.push(i) попеременно получает эти значения и корректно заполняет массив.
// В задании ниже, var i имеет уже функциональную область видимости, где после прохождения цикла i будет равно 11.
// И если i не передать в качестве аргумента, то массив заполнится значениями 11.


function varTimeout () {
    const result = [];

    for(var i = 0; i <= 10; i++) {
        setTimeout(i => result.push(i), 500, i);
    }

    return result; // числа от 0 до 10
}
module.exports = {
    calculateHours: {
        onEarth: calculateHoursOnEarth,
        onMoon: calculateHoursOnMoon,
    },
    crashMeteorite,
    createMultiplier,
    createStorage,
    createCounter,
    letTimeout,
    varTimeout
};
